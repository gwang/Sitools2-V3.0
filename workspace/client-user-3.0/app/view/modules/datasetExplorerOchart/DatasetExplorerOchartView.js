Ext.namespace('sitools.user.view.modules.datasetExplorerOchart');

Ext.define( 'sitools.user.view.modules.datasetExplorerOchart.DatasetExplorerOchartView', {
    extend: 'sitools.user.view.modules.datasetExplorerOchart.DatasetExplorerOchartViewSimple',
    //requires: 'sitools.user.view.modules.datasetExplorerOchart.TaskModel',
    alias: 'widget.DatasetExplorerOchart',

    initComponent: function(){
        var me = this;
	me.store = Ext.create("sitools.user.store.DatasetExplorerOchartTreeStore");
        /*me.store = Ext.create('Ext.data.TreeStore',{
            model: 'sitools.user.view.modules.datasetExplorerOchart.TaskModel',
            root: {
                "text": "Solar Projects",
                "leaf": false,
                expanded: true
            },
	    autoLoad: true
        });*/
	//me.store = Ext.create("sitools.user.view.modules.datasetExplorerOchart.NodesStore");
        me.chartConfig = me.chartConfig || {};
        Ext.applyIf(me.chartConfig, {
            itemTpl: [
		    '<tpl if="type == \'dataset\'">',
		      '<div class="item-dataset-body item-body" style="text-align: center">',
                      '<tpl if="imageDs">',  
			'<img src="{imageDs}" class="item-img">',
		      '</tpl>',
		      '<div class="item-title-ds item-title">{text}</div>',
		        '<a href="#" class="overDatasetService" onClick="sitools.user.utils.DatasetUtils.clickDatasetIcone(\'{url}\', \'data\');return false;">',
		          Ext.String.format('<img class="datasetochart_icon" src="{0}" data-qtip="{1}">',loadUrl.get('APP_URL') + "/common/res/images/icons/32x32/tree_datasets_32.png", i18n.get('label.dataTitle')),
		        '</a>',
		      '<div class="item-nb">({nbRecord} records)</div>',
		      '</div>',
		    '</tpl>',
		    '<tpl if="type == \'node\'">',
		      '<div class="item-node-body item-body" style="text-align: center">',
 		        '<tpl if="image.url">',
                          '<img src="{image.url}" style="height: 80px">',
                        '</tpl>',
			'<div class="item-title-node item-title">{text}</div>',
			'<tpl if="[this.checkFilter(text)] &gt; -1">',
			  '<div class="item-nb">({nbRecord} records)</div>',
			'</tpl>',
		      '</div>',
		    '</tpl>',
		    '<tpl if="type == \'rootnode\'">',
		      '<div class="item-body" style="text-align: center">',
                        '<div class="item-title-rootnode item-title">{text}</div>',
		      '</div>',
                    '</tpl>',
		{        
       		    checkFilter: function(str){
           		return str.indexOf("clickDatasetIcone");
        	    }
    		}	
		
            ],

            itemCls: 'task-item'
        });

        me.callParent( arguments );
    },
    afterRender: function(){
	var elems = document.querySelectorAll('.task-item');
        console.log(elems);
        //me.onInlineExpanderClick(null, elems);	
    },
    onItemDblClick: Ext.emptyFn
} );

