/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 * 
 * This file is part of SITools2.
 * 
 * SITools2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SITools2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SITools2. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/*global Ext, sitools, i18n, projectGlobal, alertFailure, showResponse*/

Ext.namespace('sitools.user.view.modules.missionDescription');
/**
 * ProjectDescription Module
 * @class sitools.user.modules.projectDescription
 * @extends Ext.Panel
 */
 var helpPanel =  Ext.define('sitools.user.view.modules.missionDescription.SOHOView', {
    extend : 'Ext.panel.Panel',
   
    config: {
    	// Configuration
        plain : true,
        border : false,
        bodyBorder : false,

    },
    
    constructor : function(config) {    
    	helpPanel.superclass.constructor.apply(this, arguments);
       // load the html file with ajax when the item is
       // added to the parent container
   	 this.loaded = false;
   	 // on - Appends an event handler to an observable object
   	 this.load = function () {
            if (!this.loaded && this.url && (this.url.length > 0)) {
                Ext.Ajax.request({
                    disableCaching: false,
                    url: this.url,
                    method: "GET",
                    panel: this,
                    success: function (response, request) {
                        request.panel.update(response.responseText);
                        request.panel.loaded = true;
                    },
                failure : function( response, request ) {
                    console.log("failed -- response: "+response.responseText);
                }
                });
            }
        };
        this.on('added', this.load);
        if (this.autoLoad) {
            this.load();
        }
    },
    
   url : null,
    
    
    /**
     * method called when trying to save preference
     * 
     * @returns
     */

   _getSettings : function () {
       return {
           preferencesPath : "/modules",
           preferencesFileName : this.id,
           xtype : this.$className
       };

    }
});
