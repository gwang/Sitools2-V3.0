Ext.namespace('sitools.user.modules');

Ext.define( 'sitools.user.modules.DatasetExplorerOchart', {
    extend: 'sitools.user.core.Module',


    requires: ['sitools.user.controller.modules.datasetExplorerOchart.DatasetExplorerOchartController'],
    controllers: ['sitools.user.controller.modules.datasetExplorerOchart.DatasetExplorerOchartController'],
    init : function (moduleModel) {
        
	var view = Ext.create('sitools.user.view.modules.datasetExplorerOchart.DatasetExplorerOchartView');
	this.setViewCmp(view);

        this.show(this.getViewCmp());

        this.callParent(arguments);
    },

    /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }

} );
