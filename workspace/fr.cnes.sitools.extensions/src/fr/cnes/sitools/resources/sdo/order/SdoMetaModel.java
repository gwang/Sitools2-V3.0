package fr.cnes.sitools.resources.sdo.order;

/** This Model is designed by WANG GUANJI in 19/05/2016
 *  The Model Object contains all meta data related and the extra hmi_path
 *  Add new parameters if necessary but you need attention as all data and parameters will be used in *+Resource.java
 *  
 * */
public class SdoMetaModel {
  private String series_name;
  private String recnum;
  private String sunum;
  private String wave;
  private String date_obs;
  private String pk_sdo;   
  private String new_fits_name;
  /** recnum + sunum + series_name 
   * for both AIA : sunum + series_name 
   *          HMI : recnum + series_name
   *  */
  private String pre_path_hmi;
  /**
   *  pre_path_hmi=recnum+"/"+series_name  
   **/
  
  public SdoMetaModel(){    
    /**
    this.series_name = series_name;
    this.recnum = recnum;
    this.sunum = sunum;
    this.wave = wave;
    this.date_obs = date_obs;
    this.pk_sdo = pk_sdo;
    */
  }
  
  public SdoMetaModel(String series_name, String recnum, String sunum, String wave, String date_obs, String pk_sdo, String pre_path_hmi,String new_fits_name){    
    this.series_name = series_name;
    this.recnum = recnum;
    this.sunum = sunum;
    this.wave = wave;
    this.date_obs = date_obs;
    this.pk_sdo = pk_sdo;
    this.pre_path_hmi = pre_path_hmi;
    this.new_fits_name = new_fits_name;

  }
  
  void set_series_name(String su){  
    this.series_name = su ; 
  }

  String get_series_name(){
    return series_name;
  }
  
  void set_recnum(String su){  
    this.recnum = su ; 
  }

  String get_recnum(){
    return recnum;
  }
  
  
  void set_sunum(String su){  
    this.sunum = su ; 
  }

  String get_sunum(){
    return sunum;
  }
  
  
  void set_wave(String su){  
    this.wave = su ; 
  }

  String get_wave(){
    return wave;
  }
  
  
  void set_date_obs(String su){  
    this.date_obs = su ; 
  }

  String get_date_obs(){
    return date_obs;
  }
  
  
  void set_pk_sdo(String su){  
    this.pk_sdo = su ; 
  }

  String get_pk_sdo(){    
    return pk_sdo;
  }

  public void setPre_path_hmi(String pre_path_hmi) {
    this.pre_path_hmi = pre_path_hmi;
  }

  public String getPre_path_hmi() {
    return pre_path_hmi;
  }

  public void setNew_fits_name(String new_fits_name) {
    this.new_fits_name = new_fits_name;
  }

  public String getNew_fits_name() {
    return new_fits_name;
  }
  
}