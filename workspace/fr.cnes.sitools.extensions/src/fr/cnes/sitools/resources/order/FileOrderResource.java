package fr.cnes.sitools.resources.order;

public class FileOrderResource {
  
  private String file_name;
  private String file_path_storage; // http://localhost:8182/sitools/datastorage/user/2055259/hmi.sharp_cea_720s_nrt/SUM12/D817152929/S00000 + original fits name
  private String pre_path_hmi; // the String with the format:  recnum/series_name
  
  public FileOrderResource(){
    // TODO Auto-generated constructor stub  
  }
  
  public FileOrderResource(String filename, String filepath, String file_path_hmi) {
    // TODO Auto-generated constructor stub  
    this.file_name = filename;  
    this.file_path_storage = filepath;  
    this.pre_path_hmi = file_path_hmi;
  }



  public void setPre_path_hmi(String file_path_hmi) {
    this.pre_path_hmi = file_path_hmi;
  }

  public String getPre_path_hmi() {
    return pre_path_hmi;
  }

  public void setFile_name(String file_name) {
    this.file_name = file_name;
  }

  public String getFile_name() {
    return file_name;
  }

  public void setFile_path_storage(String file_path_storage) {
    this.file_path_storage = file_path_storage;
  }

  public String getFile_path_storage() {
    return file_path_storage;
  }   
}